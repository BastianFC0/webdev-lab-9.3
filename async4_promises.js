console.log("********  TESTING ASYNCHRONOUS USING PROMISE ********* ");
//-------------------

// Consider the following data in arrays:
users= ['sara','ali', 'lim'],
courses= [['java', 'js', 'html'], ['react', 'node'], ['python','django']],
grades= [[78, 75, 88], [85, 87], [91,92]];
 
//  part 1 -- Create and consume the promise  
 
//1.1. prepare the code to be cusmed after 3 seconds
//  write a promise called promise1 - use (non arrow) anonymous function as the executor.
//  Use setTimeout that waits for 3secs. 
//  The async code inside  in setTimeouttest:
//        use a local boolean flag called error set to false
//       if no error then resolve the promise by sending a response that is an object 
//        [id='nasr', courses=[js,python,django]). 
//      Else calls then reject the promise by passing an instance of Error object (with msg  some error...)

//Promise : asynchrnous object that is either 
// resolved if success, // or  rejected if problem

//produce code of the  promise (promise1)

let promise1 = new Promise(function (response, rejected){
  let error = true;
  if(!error){
    response([id='nasr', courses=['js','python','django']]);
  }else{
    rejected(new Error('Oops'));
  }});
  promise1.then(resp=>console.log('promise resolved', resp)).catch(err=>console.log('promise rejected', err));




//1.2 cosume the promise
// consume the promise promise1 by invoking the then and catch methods.
// use an arrow function in both then and catch
// in the then method log the response
// in the catch method log the error
// test both cases by flipping the local error flag in the code 1.1

//consume the code of the resolved promise





//--------------------------------------------------------
  //  part 2 -- mULTIPLE  promises (chaining) 
// Consider the following data in arrays:
 users= [userid='ssara', name= 'sara sears', age=20, userid='zali', name= 'zaki ali', age=18] ;
// userid='hling', name= 'hua ling'; age=19 , userid='kraja', name= 'kumar raja', age=25 
// courses= [['java', 'js', 'html'], ['react', 'node'], ['python','django'], ['c#', 'c++', 'node']]
// grades= [[78, 75, 88], [85, 87], [91,92], [71,78,81]];

//2.1 Write a function called getData with 2 args (data and a boolean error with default value set to false)
// the function getData returns a promise that uses a setTimeout (of 1 second) to resolve data if no error.
// otherwise reject the promise with message  'something wrong with  data'





//2.2 test getData with users, and 
//  -when resolved display all users with message promise users resolved.
//  - when rejecting the promise, display  message  'something wrong with  data'
//  - test both cases - // YOU MUST OPEN devtools and run step by step to see how the code execute at the low level





//2.2 chain the promise to get  getData of user zaki ali-
// display jis name, courses and average score
users= [userid='ssara', name= 'sara sears', age=20, userid='zali', name= 'zaki ali', age=18] ;
function getData(data, er=false){
let promise2 = new Promise(function(resolved, rejected){
  let error = er;
  if(!error){
    setTimeout(
    resolved([users[1],users[4]]),1000);
  }else{
    rejected(new Error('something wrong with data'));
  }
})
promise2.then(resp=>console.log('Success', resp)).catch(err=>console.log('something went wrong', err))
};

